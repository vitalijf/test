<%@ page isELIgnored="false" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/mykidslist.css'>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href='${pageContext.request.contextPath}/resources/css/fullcalendar.css' rel='stylesheet'/>
<link href='${pageContext.request.contextPath}/resources/css/fullcalendar.print.css' rel='stylesheet' media='print'/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

<script src="${pageContext.request.contextPath}/resources/js/socialnetworks.js"></script>


<div class="list">
    <div class="kidslistblock">
        <div class="kidslistitem">
            <div class="thumb">
                <img src="${pageContext.request.contextPath}/resources/img/facebook-logo.png" width="100" height="100"/>
            </div>
            <button id="facebook-connect-button" onclick="generateFaceBooklink()" class="btn btn-danger btn-lg">Connect</button>
            <button id="facebook-configure-button" onclick="fetchFacebookSettings()" class="btn btn-success btn-lg">Configure</button>
        </div>
    </div>
    <div class="kidslistblock">
        <div class="kidslistitem">
            <div class="thumb">
                <img src="${pageContext.request.contextPath}/resources/img/linkedin-logo.png" width="100" height="100"/>
            </div>
            <button class="btn btn-danger btn-lg" onclick="getnerateLinkedInLink()">Connect</button>
            <button id="linkedin-configure-button" onclick="fetchLinkedinSettings()" class="btn btn-success btn-lg">Configure</button>
        </div>
    </div>
    <div class="kidslistblock">
        <div class="kidslistitem">
            <div class="thumb">
                <img src="${pageContext.request.contextPath}/resources/img/twitter-logo.png" width="100" height="100"/>
            </div>
            <button class="btn btn-danger btn-lg" onclick="generateTwitterLink()">Connect</button>
            <button id="twitter-configure-button" onclick="fetchTwitterSettings()" class="btn btn-success btn-lg">Configure</button>
        </div>
    </div>

    <div align="center">
        <div id="facebook-params" class="dialog" style="width: 500px" hidden >
            <form id="recurrent-booking-form">
                <div class="form-group">
                    <label for="minimum-sharing-friends-fb">
                        Minimum friends
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control"
                               id="minimum-sharing-friends-fb">
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="minimum-same-teams-number">
                        Same teams
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control " id="minimum-same-teams-number">
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="max-year-difference-number">
                      Year difference
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control " id="max-year-difference-number">
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="min-same-likes-number">
                        Same likes count
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control " id="min-same-likes-number">
                    </div>
                </div>
                <br>


                <div class="col-xs-12">
                    <div class="row">
                        <form role="form">
                            <div class="row col-xs-5 choose-booking">
                                <br>
                                <div class="radio-button">
                                    <label><input type="radio" name="MALE"
                                                  id="female-fb"
                                                  class="gender-radio" checked>
                                      Male
                                    </label>
                                </div>
                                <div class="radio-button">
                                    <label><input type="radio" name="FEMALE"
                                                  id="male-fb"
                                                  class="gender-radio">
                                        Female
                                    </label>
                                </div>

                                <div class="radio-button">
                                    <label><input type="radio" name=""
                                                  id="ignore"
                                                  class="gender-radio">
                                      Ignore
                                    </label>
                                </div>

                            </div>
                        </form>

                        <div class="clearfix"></div>
                        <br>
                        <div class="clearfix">
                            <button type="button" class="btn btn-success" onclick="updateFacebookParams()" id="update-recurrent-booking"
                                    hidden="true">
                               Save
                            </button>
                        </div>
                        <br>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div align="center">
        <div id="linkedin-params" class="dialog" style="width: 500px" hidden >

            <div class="form-group">
                    <label for="minimum-connections-number-lk">
                        Minimum connections
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control " id="minimum-connections-number-lk">
                    </div>
            </div>
            <br>

            <div class="form-group">
                <label for="location-lk">
                   Location
                </label>
                <br>
                <div class="col-xs-6 choose-booking">
                    <input type="text" class="text-center form-control " id="location-lk">
                </div>
            </div>
            <br>

            <div class="form-group">
                <label for="position-lk">
                    Position
                </label>
                <br>
                <div class="col-xs-6 choose-booking">
                    <input type="text" class="text-center form-control " id="position-lk">
                </div>
            </div>
            <br>

            <div class="form-group">
                <label for="industry-lk">
                    Industry
                </label>
                <br>
                <div class="col-xs-6 choose-booking">
                    <input type="text" class="text-center form-control " id="industry-lk">
                </div>
            </div>
            <br>
            <div class="clearfix"></div>
            <br>
            <div class="clearfix">
                <button type="button" class="btn btn-success" onclick="updateLinkedinParams()"
                        hidden="true">
                    Save
                </button>
            </div>
            <br>


        </div>
    </div>

    <div align="center">
        <div id="twitter-params" class="dialog" style="width: 500px" hidden >
            <form>
                <div class="form-group">
                    <label for="minimum-friends-tw">
                        Minimum friends
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control"
                               id="minimum-friends-tw">
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="minimum-followers-tw">
                        Minimum followers
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control"
                               id="minimum-followers-tw">
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="minimum-statuses-tw">
                        Minimum statuses
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control"
                               id="minimum-statuses-tw">
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="minimum-favorites-tw">
                        Minimum favorites
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control"
                               id="minimum-favorites-tw">
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="language-tw">
                        Minimum friends
                    </label>
                    <br>
                    <div class="col-xs-6 choose-booking">
                        <input type="number" class="text-center form-control"
                               id="language-tw">
                    </div>
                </div>
                <br>


                        </form>

                        <div class="clearfix"></div>
                        <br>
                        <div class="clearfix">
                            <button type="button" class="btn btn-success" onclick="updateTwitterParams()"
                                    hidden="true">
                                Save
                            </button>
                        </div>
                        <br>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


