$(document).ready(function(){

});

function getAllFromFacebook() {
    const url = '/suggested/facebook';
    makeAjaxGetRequest(url, fillSuggestedBlock, errorOnSuggested);
}

function getAllFromTwitter() {
    const url = '/suggested/twitter';
    makeAjaxGetRequest(url, fillSuggestedBlock, errorOnSuggested);
}

function getAllFromAllFromLinkedin() {
    const url = '/suggested/linkedin';
    makeAjaxGetRequest(url, fillSuggestedBlock, errorOnSuggested);
}

function getAll() {
    const url = '/suggested/all';
    makeAjaxGetRequest(url, fillSuggestedBlock, errorOnSuggested);
}

function fillSuggestedBlock(data) {
    let content = '';
    $.each(data, (index, element) => {
        content += '<div class="kidslistblock">'
            + '<div class="kidslistitem">'
            + '<div class="thumb">' + element.full_name + '</div>'
            + '<button class="btn btn-danger btn-lg" href="' + data.facebook_link + '">FB</button>'
            + '<button class="btn btn-danger btn-lg" href="' + data.linkedin_link + '">LK</button>'
            + '<button class="btn btn-danger btn-lg" href="' + data.twitter_link + '">TW</button>'
            + '</div>'
            + '</div>'
    });
    $('#class-list').html(content);

}

function errorOnSuggested() {
    alert('ErrorOnSuggested')
}

