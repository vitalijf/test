
$(document).ready(function () {
});

function fetchFacebookSettings() {
    $("#facebook-params").dialog();
}

function fetchLinkedinSettings() {
    $("#linkedin-params").dialog();
}

function fetchTwitterSettings() {
    $("#twitter-params").dialog();
}

function generateTwitterLink() {
    const url = '/twitter/authorizationLink';
    makeAjaxGetRequest(url, null, twitterSecondStep, twitterError);
}

function generateFaceBooklink() {
    const url = '/facebook/authorizationLink';
    makeAjaxGetRequest(url, null, facebookSecondStep, facebookError);
}

function getnerateLinkedInLink() {
    const url = '/linkedin/authorizationLink';
    makeAjaxGetRequest(url, null, linkedinSecondStep, linkedinError);
}

function updateFacebookParams() {
    const minFriendsNumbers = $('#minimum-sharing-friends-fb').val();
    const minSameTeam = $('#minimum-same-teams-number').val();
    const maxYearDifference = $('#max-year-difference-number').val();
    const minSameLikes = $('#min-same-likes-number').val();
    const gender = $('.gender-radio:checked').attr('name');
    const params = {};
    if (minFriendsNumbers) {
        params.minFriendsNumbers = minFriendsNumbers;
    }
    if (minSameTeam) {
        params.minSameTeam = minSameTeam;
    }
    if (maxYearDifference) {
        params.maxYearDifference = maxYearDifference;
    }
    if (minSameLikes) {
        params.minSameLikes = minSameLikes;
    }
    if (gender) {
        params.gender = gender;
    }
    makeAjaxPostRequest('/facebook/params', params, afterFBParamsSaving, errorOnFBParamsSaving);
}

function updateLinkedinParams() {
    const minConnections = $('#minimum-connections-number-lk').val();
    const location = $('#location-lk').val();
    const industry = $('#industry-lk').val();
    const position = $('#position-lk').val();
    const params = {};
    if (minConnections) {
        params.minConnections = minConnections;
    }
    if (location) {
        params.location = location;
    }
    if (industry) {
        params.industry = industry;
    }
    if (position) {
        params.position = position;
    }
    makeAjaxPostRequest('/linkedin/params', params, afterLKParamsSaving, errorOnLKParamsSaving);
}

function updateTwitterParams() {
    const minFriendsCount = $('#minimum-friends-tw').val();
    const minFollowersCount = $('#minimum-followers-tw').val();
    const minFavoritesCount =$('#minimum-favorites-tw').val();
    const minStatusesCount = $('#minimum-statuses-tw').val();
    const language = $('#language-tw').val();
    const params = {};
    if (minFriendsCount) {
        params.minFriendsCount = minFriendsCount;
    }
    if (minFollowersCount) {
        params.minFollowersCount = minFollowersCount;
    }
    if (minFavoritesCount) {
        params.minFavoritesCount = minFavoritesCount;
    }
    if (minStatusesCount) {
        params.minStatusesCount = minStatusesCount;
    }
    if (language) {
        params.language = language;
    }
    makeAjaxPostRequest('/twitter/params', params, afterTWParamsSaving, errorOnTWParamsSaving);
}

function afterFBParamsSaving() {
    $('#facebook-params').dialog('close');
    alert('Params have been successfully saved');
}

function afterLKParamsSaving() {
    $('#linkedin-params').dialog('close');
    alert('Params have been successfully saved');
}

function afterTWParamsSaving() {
    $('#twitter-params').dialog('close');
    alert('Params have been successfully saved');
}

function linkedinSecondStep(link) {
    window.open(link, '_blank');
}

function facebookSecondStep(link) {
    window.open(link, '_blank');
}

function twitterSecondStep(link) {
    window.open(link, '_blank');
}

function facebookError() {
    alert('Facebook error')
}

function twitterError() {
    alert('Twitter error')
}

function linkedinError() {
    alert('linkedin error');
}

function errorOnFBParamsSaving() {
    alert('fb params saving error')
}

function errorOnLKParamsSaving() {
    alert('lk params saving error');
}

function errorOnTWParamsSaving() {
    alert('twitter params saving error');
}