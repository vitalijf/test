function makeAjaxGetRequest(urlID, params, callback, errorCallback) {
    $.ajax({
        url: urlID,
        type: "get",
        data: params,
        success: function (data) {
            callback(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errorCallback(jqXHR, textStatus, errorThrown);
        }
    });
}

function makeNonAsyncGetRequest(urlID, params, callback, errorCallback) {
    $.ajax({
        url: urlID,
        type: "get",
        async: false,
        data: params,
        success: function (data) {
            callback(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errorCallback(jqXHR, textStatus, errorThrown);
        }
    });
}


function makeAjaxPostRequest(urlID, params, callback, errorCallback) {
    $.ajax({
        url: urlID,
        data: JSON.stringify(params),
        dataType: "json",
        type: "post",
        contentType: "application/json; charset=UTF-8",
        success: function (data) {
            callback(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errorCallback(jqXHR, textStatus, errorThrown);
        }
    });
}

function makeAjaxPutRequest(urlID, params, callback, errorCallback) {
    $.ajax({
        url: urlID,
        data: JSON.stringify(params),
        dataType: "json",
        type: "put",
        contentType: "application/json; charset=UTF-8",
        success: function (data) {
            callback(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errorCallback(jqXHR, textStatus, errorThrown);
        }
    });
}

function makeAjaxDeleteRequest(urlID, params, callback, errorCallback) {
    $.ajax({
        url: urlID,
        data: JSON.stringify(params),
        dataType: "json",
        type: "delete",
        contentType: "application/json; charset=UTF-8",
        success: function (data) {
            callback(data)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errorCallback(jqXHR, textStatus, errorThrown);
        }
    });
}

function makeAjaxPatchRequest(urlID, params,  callback, errorCallback) {
    $.ajax({
        url: urlID,
        data: JSON.stringify(params),
        dataType: "json",
        type: "patch",
        contentType: "application/json; charset=UTF-8",
        success: function (data) {
            callback(data)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errorCallback(jqXHR, textStatus, errorThrown);
        }
    });
}
