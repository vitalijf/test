package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "shared_connections")
public class SharedConnections {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "first_user_id")
    private User firstUser;

    @ManyToOne
    @JoinColumn(name = "second_user_id")
    private User secondUser;

    @Column(name = "social_network")
    @Enumerated(value = EnumType.STRING)
    private SocialNetwork socialNetwork;




}
