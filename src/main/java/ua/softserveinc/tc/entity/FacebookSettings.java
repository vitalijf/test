package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "facebook_settings")
public class FacebookSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "minimum_friends_number")
    private Long minimumFriendsNumber;

    @Column(name = "minimum_same_teams")
    private Long minimumSameTeams;

    @Column(name = "maximum_year_difference")
    private Long maximumYearDifference;

    @Column(name = "minimum_same_likes")
    private Long minimumSameLikes;

    @Column(name = "gender")
    private String gender;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMinimumFriendsNumber() {
        return minimumFriendsNumber;
    }

    public void setMinimumFriendsNumber(Long minimumFriendsNumber) {
        this.minimumFriendsNumber = minimumFriendsNumber;
    }

    public Long getMinimumSameTeams() {
        return minimumSameTeams;
    }

    public void setMinimumSameTeams(Long minimumSameTeams) {
        this.minimumSameTeams = minimumSameTeams;
    }

    public Long getMaximumYearDifference() {
        return maximumYearDifference;
    }

    public void setMaximumYearDifference(Long maximumYearDifference) {
        this.maximumYearDifference = maximumYearDifference;
    }

    public Long getMinimumSameLikes() {
        return minimumSameLikes;
    }

    public void setMinimumSameLikes(Long minimumSameLikes) {
        this.minimumSameLikes = minimumSameLikes;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
