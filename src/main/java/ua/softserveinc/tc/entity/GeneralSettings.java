package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "general_setting")
public class GeneralSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "facebook_actuality")
    private Double facebookActuality;

    @Column(name = "twitter_actuality")
    private Double twitterActuality;

    @Column(name = "linkedin_actuality")
    private Double linkedinActuality;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFacebookActuality() {
        return facebookActuality;
    }

    public void setFacebookActuality(Double facebookActuality) {
        this.facebookActuality = facebookActuality;
    }

    public Double getTwitterActuality() {
        return twitterActuality;
    }

    public void setTwitterActuality(Double twitterActuality) {
        this.twitterActuality = twitterActuality;
    }

    public Double getLinkedinActuality() {
        return linkedinActuality;
    }

    public void setLinkedinActuality(Double linkedinActuality) {
        this.linkedinActuality = linkedinActuality;
    }
}
