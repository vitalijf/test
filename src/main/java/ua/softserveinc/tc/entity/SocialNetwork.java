package ua.softserveinc.tc.entity;

public enum  SocialNetwork {

    FACEBOOK, LINKEDIN, TWITTER
}
