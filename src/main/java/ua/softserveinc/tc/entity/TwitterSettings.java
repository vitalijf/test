package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "twitter_settings")
public class TwitterSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "minimum_friends_count")
    private Long minimumFriendsCount;

    @Column(name = "minimum_followers_count")
    private Long minimumFollowersCount;

    @Column(name = "minimum_favorites_count")
    private Long minimumFavoritesCount;

    @Column(name = "minimum_statuses_count")
    private Long minimumStatusesCount;

    @Column(name = "language")
    private String language;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMinimumFriendsCount() {
        return minimumFriendsCount;
    }

    public void setMinimumFriendsCount(Long minimumFriendsCount) {
        this.minimumFriendsCount = minimumFriendsCount;
    }

    public Long getMinimumFollowersCount() {
        return minimumFollowersCount;
    }

    public void setMinimumFollowersCount(Long minimumFollowersCount) {
        this.minimumFollowersCount = minimumFollowersCount;
    }

    public Long getMinimumFavoritesCount() {
        return minimumFavoritesCount;
    }

    public void setMinimumFavoritesCount(Long minimumFavoritesCount) {
        this.minimumFavoritesCount = minimumFavoritesCount;
    }

    public Long getMinimumStatusesCount() {
        return minimumStatusesCount;
    }

    public void setMinimumStatusesCount(Long minimumStatusesCount) {
        this.minimumStatusesCount = minimumStatusesCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
