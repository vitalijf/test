package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "suggested_friends")
public class SuggestedFriends {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "target_user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "suggested_user_id")
    private User suggestedUser;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private SuggestedStatus suggestedStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getSuggestedUser() {
        return suggestedUser;
    }

    public void setSuggestedUser(User suggestedUser) {
        this.suggestedUser = suggestedUser;
    }

    public SuggestedStatus getSuggestedStatus() {
        return suggestedStatus;
    }

    public void setSuggestedStatus(SuggestedStatus suggestedStatus) {
        this.suggestedStatus = suggestedStatus;
    }
}
