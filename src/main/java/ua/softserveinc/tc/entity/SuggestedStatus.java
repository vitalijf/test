package ua.softserveinc.tc.entity;

public enum  SuggestedStatus {

    CONFIRMED, DECLINED, WITHOUT_DECISION
}
