package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "social_data")
public class SocialData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "facebook_data_id")
    private FacebookData facebookData;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "linkedin_data_id")
    private LinkedinData linkedinData;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "twitter_data_id")
    private TwitterData twitterData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FacebookData getFacebookData() {
        return facebookData;
    }

    public void setFacebookData(FacebookData facebookData) {
        this.facebookData = facebookData;
    }

    public LinkedinData getLinkedinData() {
        return linkedinData;
    }

    public void setLinkedinData(LinkedinData linkedinData) {
        this.linkedinData = linkedinData;
    }

    public TwitterData getTwitterData() {
        return twitterData;
    }

    public void setTwitterData(TwitterData twitterData) {
        this.twitterData = twitterData;
    }
}
