package ua.softserveinc.tc.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "facebook_data")
public class FacebookData {

    @Id
    private Long id;

    @Column(name = "friends_count")
    private Long friendsCount;

    @Column(name = "birthday")
    private LocalDate birthDay;

    @Column(name = "gender")
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @OneToMany(mappedBy = "facebookData",cascade = {CascadeType.ALL})
    private List<FacebookTeam> facebookTeams;

    @OneToMany(mappedBy = "facebookData",cascade = {CascadeType.ALL})
    private List<FacebookLike> facebookLikes;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(Long friendsCount) {
        this.friendsCount = friendsCount;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<FacebookTeam> getFacebookTeams() {
        return facebookTeams;
    }

    public void setFacebookTeams(List<FacebookTeam> facebookTeams) {
        this.facebookTeams = facebookTeams;
    }

    public List<FacebookLike> getFacebookLikes() {
        return facebookLikes;
    }

    public void setFacebookLikes(List<FacebookLike> facebookLikes) {
        this.facebookLikes = facebookLikes;
    }
}
