package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "social_settings")
public class SocialSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "general_setting_id")
    private GeneralSettings generalSettings;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "facebook_setting_id")
    private FacebookSettings faceBookSettings;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "twitter_setting_id")
    private TwitterSettings twitterSettings;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "linkedin_setting_id")
    private LinkedinSettings linkedinSettings;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GeneralSettings getGeneralSettings() {
        return generalSettings;
    }

    public void setGeneralSettings(GeneralSettings generalSettings) {
        this.generalSettings = generalSettings;
    }

    public FacebookSettings getFaceBookSettings() {
        return faceBookSettings;
    }

    public void setFaceBookSettings(FacebookSettings faceBookSettings) {
        this.faceBookSettings = faceBookSettings;
    }

    public TwitterSettings getTwitterSettings() {
        return twitterSettings;
    }

    public void setTwitterSettings(TwitterSettings twitterSettings) {
        this.twitterSettings = twitterSettings;
    }

    public LinkedinSettings getLinkedinSettings() {
        return linkedinSettings;
    }

    public void setLinkedinSettings(LinkedinSettings linkedinSettings) {
        this.linkedinSettings = linkedinSettings;
    }
}
