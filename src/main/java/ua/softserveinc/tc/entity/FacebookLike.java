package ua.softserveinc.tc.entity;

import javax.persistence.*;

@Entity
@Table(name = "facebook_like")
public class FacebookLike {

    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "facebook_data_id")
    private FacebookData facebookData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FacebookData getFacebookData() {
        return facebookData;
    }

    public void setFacebookData(FacebookData facebookData) {
        this.facebookData = facebookData;
    }
}
