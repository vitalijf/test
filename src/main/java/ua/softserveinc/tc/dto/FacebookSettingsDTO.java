package ua.softserveinc.tc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookSettingsDTO {

    @JsonProperty("minFriendsNumbers")
    private Long minFriendsNumbers;

    @JsonProperty("minSameTeam")
    private Long minSameTeam;

    @JsonProperty("maxYearDifference")
    private Long maxYearDifference;

    @JsonProperty("minSameLikes")
    private Long minSameLikes;

    @JsonProperty("gender")
    private String gender;

    public Long getMinFriendsNumbers() {
        return minFriendsNumbers;
    }

    public void setMinFriendsNumbers(Long minFriendsNumbers) {
        this.minFriendsNumbers = minFriendsNumbers;
    }

    public Long getMinSameTeam() {
        return minSameTeam;
    }

    public void setMinSameTeam(Long minSameTeam) {
        this.minSameTeam = minSameTeam;
    }

    public Long getMaxYearDifference() {
        return maxYearDifference;
    }

    public void setMaxYearDifference(Long maxYearDifference) {
        this.maxYearDifference = maxYearDifference;
    }

    public Long getMinSameLikes() {
        return minSameLikes;
    }

    public void setMinSameLikes(Long minSameLikes) {
        this.minSameLikes = minSameLikes;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
