package ua.softserveinc.tc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkedinParamsDTO {

    @JsonProperty("minConnections")
    private Long minConnections;

    @JsonProperty("industry")
    private String industry;

    @JsonProperty("location")
    private String location;

    @JsonProperty("position")
    private String position;

    public Long getMinConnections() {
        return minConnections;
    }

    public void setMinConnections(Long minConnections) {
        this.minConnections = minConnections;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
