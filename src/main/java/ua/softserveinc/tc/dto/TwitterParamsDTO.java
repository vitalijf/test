package ua.softserveinc.tc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterParamsDTO {

    @JsonProperty("minFriendsCount")
    private Long minFriendsCount;

    @JsonProperty("minFollowersCount")
    private Long minFollowersCount;

    @JsonProperty("minFavoritesCount")
    private Long minFavoritesCount;

    @JsonProperty("minStatusesCount")
    private Long minStatusesCount;

    @JsonProperty("language")
    private String language;

    public Long getMinFriendsCount() {
        return minFriendsCount;
    }

    public void setMinFriendsCount(Long minFriendsCount) {
        this.minFriendsCount = minFriendsCount;
    }

    public Long getMinFollowersCount() {
        return minFollowersCount;
    }

    public void setMinFollowersCount(Long minFollowersCount) {
        this.minFollowersCount = minFollowersCount;
    }

    public Long getMinFavoritesCount() {
        return minFavoritesCount;
    }

    public void setMinFavoritesCount(Long minFavoritesCount) {
        this.minFavoritesCount = minFavoritesCount;
    }

    public Long getMinStatusesCount() {
        return minStatusesCount;
    }

    public void setMinStatusesCount(Long minStatusesCount) {
        this.minStatusesCount = minStatusesCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
