package ua.softserveinc.tc.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ua.softserveinc.tc.entity.SocialData;
import ua.softserveinc.tc.entity.TwitterData;
import ua.softserveinc.tc.entity.User;

import java.util.Objects;
import java.util.Optional;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SuggestedFriendDTO {

    @JsonIgnore
    private String email;

    @JsonProperty(value = "full_name")
    private String fullName;

    @JsonProperty(value = "twitter_link")
    private String twitterLink;

    @JsonProperty(value = "linkedin_link")
    private String linkedinLink;

    @JsonProperty(value = "facebook_link")
    private String facebookLink;

    public SuggestedFriendDTO(User user) {
        email = user.getEmail();
        fullName = user.getFullName();

        twitterLink = Optional.ofNullable(user.getSocialData())
                    .map(d -> Optional.ofNullable(d.getTwitterData())
                            .map(t -> "https://twitter.com/" + t.getId())
                            .orElse(null))
                .orElse(null);

        linkedinLink = Optional.ofNullable(user.getSocialData())
                .map(d -> Optional.ofNullable(d.getLinkedinData())
                        .map(t -> "https://linkedin.com/" + t.getId())
                        .orElse(null))
                .orElse(null);

        facebookLink = Optional.ofNullable(user.getSocialData())
                .map(d -> Optional.ofNullable(d.getFacebookData())
                        .map(t -> "https://facebook.com/" + t.getId())
                        .orElse(null))
                .orElse(null);
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getLinkedinLink() {
        return linkedinLink;
    }

    public void setLinkedinLink(String linkedinLink) {
        this.linkedinLink = linkedinLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuggestedFriendDTO that = (SuggestedFriendDTO) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
