package ua.softserveinc.tc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.softserveinc.tc.dto.LinkedinParamsDTO;
import ua.softserveinc.tc.dto.TwitterParamsDTO;
import ua.softserveinc.tc.service.LinkedinService;
import ua.softserveinc.tc.service.TwitterService;

@Controller
@RequestMapping("/twitter")
public class TwitterController {

    @Autowired
    private TwitterService twitterService;

    @GetMapping("/token")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createAccessToken(@RequestParam("oauth_token") String oauthToken, @RequestParam("oauth_verifier") String verifier){
        twitterService.createAccessToken(oauthToken, verifier);
    }

    @GetMapping("/authorizationLink")
    @ResponseBody
    public String createFacebookAuthorization(){
        return twitterService.createAuthorizationUrl();
    }

    @PostMapping("/params")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveParams(@RequestBody TwitterParamsDTO dto){
        twitterService.saveParams(dto);
    }

}