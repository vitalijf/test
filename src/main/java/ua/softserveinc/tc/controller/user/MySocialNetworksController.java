package ua.softserveinc.tc.controller.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ua.softserveinc.tc.constants.ChildConstants;
import ua.softserveinc.tc.entity.Child;
import ua.softserveinc.tc.entity.Role;
import ua.softserveinc.tc.entity.User;
import ua.softserveinc.tc.service.UserService;

import java.security.Principal;
import java.util.List;


/**
 * Controller handles request for "My Kids" view
 */

@Controller
public class MySocialNetworksController {

    @Autowired
    private UserService userService;

    @GetMapping(ChildConstants.View.MY_SOCIAL_NETWORKS)
    public ModelAndView mySocialNetworks(Principal principal)
    {
        ModelAndView model = new ModelAndView();
        model.setViewName(ChildConstants.View.MY_SOCIAL_NETWORKS);
        User current = userService.getUserByEmail(principal.getName());

        return model;
    }
}