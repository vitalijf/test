package ua.softserveinc.tc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.softserveinc.tc.dto.SuggestedFriendDTO;
import ua.softserveinc.tc.service.SuggestedService;

import java.util.List;

@RestController
@RequestMapping("/suggested")
public class SuggestedController {

    @Autowired
    private SuggestedService suggestedService;

    @RequestMapping("/twitter")
    public List<SuggestedFriendDTO> getAllFromTwitter() {
        return suggestedService.getAllFromTwitter();
    }

    @RequestMapping("/linkedin")
    public List<SuggestedFriendDTO> getAllFromLinkedin() {
        return suggestedService.getAllFromLinkedin();
    }

    @RequestMapping("/facebook")
    public List<SuggestedFriendDTO> getAllFromFacebook() {
        return suggestedService.getAllFromFacebook();
    }

    @RequestMapping("/all")
    public List<SuggestedFriendDTO> getAll() {
        return suggestedService.getAll();
    }

}
