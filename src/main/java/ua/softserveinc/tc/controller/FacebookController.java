package ua.softserveinc.tc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.softserveinc.tc.dto.FacebookSettingsDTO;
import ua.softserveinc.tc.service.FacebookService;

@Controller
@RequestMapping("/facebook")
public class FacebookController {

    @Autowired
    private FacebookService facebookService;

    @GetMapping("/token")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createFacebookAccessToken(@RequestParam("code") String code){
         facebookService.createFacebookAccessToken(code);
    }

    @PostMapping("/params")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveUserParams(@RequestBody FacebookSettingsDTO dto){
        facebookService.saveFacebookSettings(dto);
    }

    @GetMapping("/authorizationLink")
    @ResponseBody
    public String createFacebookAuthorization(){
        return facebookService.createFacebookAuthorizationURL();
    }

    @GetMapping("/getName")
    public String getNameResponse(){
//        return facebookService.getName();
        return null;
    }

}