package ua.softserveinc.tc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.softserveinc.tc.dto.LinkedinParamsDTO;
import ua.softserveinc.tc.service.LinkedinService;

@Controller
@RequestMapping("/linkedin")
public class LinkedinController {

    @Autowired
    private LinkedinService linkedinService;

    @GetMapping("/token")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createFacebookAccessToken(@RequestParam("code") String code){
        linkedinService.createAccessToken(code);
    }

    @GetMapping("/authorizationLink")
    @ResponseBody
    public String createFacebookAuthorization(){
        return linkedinService.createAuthorizationUrl();
    }

    @PostMapping("/params")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveParams(@RequestBody LinkedinParamsDTO dto){
        linkedinService.saveParams(dto);
    }

}