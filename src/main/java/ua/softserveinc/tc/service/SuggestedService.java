package ua.softserveinc.tc.service;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.softserveinc.tc.dao.SuggestedDAO;
import ua.softserveinc.tc.dao.UserDao;
import ua.softserveinc.tc.dto.SuggestedFriendDTO;
import ua.softserveinc.tc.entity.*;
import ua.softserveinc.tc.util.Security;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class SuggestedService {


    @Autowired
    private UserDao userDao;

    @Autowired
    private SuggestedDAO suggestedDAO;

    public List<SuggestedFriendDTO> getAllFromTwitter() {
        User user = userDao.getUserByEmail(Security.getUserEmail());
        TwitterSettings twitterSettings = Optional.ofNullable(user.getSocialSettings())
                .map(SocialSettings::getTwitterSettings).orElse(null);
        List<User> _1 = new ArrayList<>();
        List<User> _2 = new ArrayList<>();
        List<User> _3 = new ArrayList<>();
        List<User> _4 = new ArrayList<>();
        List<User> _5 = new ArrayList<>();
        if (twitterSettings == null) {
            return new ArrayList<>();
        }
        String language = twitterSettings.getLanguage();
        if (language != null) {
          _1 = suggestedDAO.getAllWithLanguage(language, user.getId());
        }

        Long minFriendsCount = twitterSettings.getMinimumFriendsCount();
        if (minFriendsCount != null) {
           _2 = suggestedDAO.getAllWithMinFriendsCount(minFriendsCount, user.getId());
        }

        Long minStatusesCount = twitterSettings.getMinimumStatusesCount();
        if (minStatusesCount != null) {
            _3 = suggestedDAO.getAllWithMinStatusesCount(minStatusesCount, user.getId());
        }

        Long minFollowersCount = twitterSettings.getMinimumFollowersCount();
        if (minFollowersCount != null) {
            _4 = suggestedDAO.getAllWithMinFollowersCount(minFollowersCount, user.getId());
        }

        Long minFavorites = twitterSettings.getMinimumFavoritesCount();
        if (minFavorites != null) {
            _5 = suggestedDAO.getAllWithMinFavoritesCount(minFavorites, user.getId());
        }
        List<User> intersection = intersection(_1, _2, _3, _4, _5);
        return intersection.stream().map(SuggestedFriendDTO::new).collect(Collectors.toList());

    }

    public List<SuggestedFriendDTO> getAllFromLinkedin() {
        User user = userDao.getUserByEmail(Security.getUserEmail());
        LinkedinSettings linkedinSettings = Optional.ofNullable(user.getSocialSettings())
                .map(SocialSettings::getLinkedinSettings).orElse(null);
        List<User> _1 = new ArrayList<>();
        List<User> _2 = new ArrayList<>();
        List<User> _3 = new ArrayList<>();
        List<User> _4 = new ArrayList<>();
        if (linkedinSettings == null) {
            return new ArrayList<>();
        }
        String industry = linkedinSettings.getIndustry();
        if (industry != null) {
            _1 = suggestedDAO.getAllWithIndustry(industry, user.getId());
        }

        String location = linkedinSettings.getLocation();
        if (location != null) {
            _2 = suggestedDAO.getAllWithLocation(location, user.getId());
        }

        String position = linkedinSettings.getPosition();
        if (position != null) {
            _3 = suggestedDAO.getAllWithPosition(position, user.getId());
        }

        Long minConnections = linkedinSettings.getMinimumConnections();
        if (minConnections != null) {
            _4 = suggestedDAO.getAllWithMinConnections(minConnections, user.getId());
        }

        List<User> intersection = intersection(_1, _2, _3, _4);
        return intersection.stream().map(SuggestedFriendDTO::new).collect(Collectors.toList());
    }

    public List<SuggestedFriendDTO> getAllFromFacebook() {
        User user = userDao.getUserByEmail(Security.getUserEmail());
        FacebookSettings facebookSettings = Optional.ofNullable(user.getSocialSettings())
                .map(SocialSettings::getFaceBookSettings).orElse(null);
        List<User> _1 = new ArrayList<>();
        List<User> _2 = new ArrayList<>();
        List<User> _3 = new ArrayList<>();
        List<User> _4 = new ArrayList<>();
        List<User> _5 = new ArrayList<>();
        if (facebookSettings == null) {
            return new ArrayList<>();
        }
        String gender = facebookSettings.getGender();
        if (gender != null) {
            _1 = suggestedDAO.getAllWithGender(gender, user.getId());
        }

        Long minFriendsCount = facebookSettings.getMinimumFriendsNumber();
        if (minFriendsCount != null) {
            _2 = suggestedDAO.getAllWithMinFriendsCountFB(minFriendsCount, user.getId());
        }

        Long maximumYearDifference = facebookSettings.getMaximumYearDifference();
        if (maximumYearDifference != null) {
            _3 = suggestedDAO.getAllWithMaximumYearDifference(maximumYearDifference, user);
        }

        Long minimumSameLikes = facebookSettings.getMinimumSameLikes();
        if (minimumSameLikes != null) {
            _4 = suggestedDAO.getAllWithSameLikes(minimumSameLikes, user);
        }

        Long minimumSameTeams = facebookSettings.getMinimumSameTeams();
        if (minimumSameTeams != null) {
            _5 = suggestedDAO.getAllWithMiniSameTeams(minimumSameTeams, user);
        }
        List<User> intersection = intersection(_1, _2, _3, _4, _5);
        return intersection.stream().map(SuggestedFriendDTO::new).collect(Collectors.toList());
    }

    public List<SuggestedFriendDTO> getAll() {
        return intersection(getAllFromFacebook(), getAllFromLinkedin(), getAllFromTwitter());
    }

    private <T> List<T> intersection(List<T>... list) {
        Set<T> result = Sets.newHashSet(list[0]);
        for (List<T> numbers : list) {
            result = Sets.intersection(result, Sets.newHashSet(numbers));
        }
        return new ArrayList<>(result);
    }

}
