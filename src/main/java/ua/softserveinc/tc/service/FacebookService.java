package ua.softserveinc.tc.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.softserveinc.tc.dao.UserDao;
import ua.softserveinc.tc.dto.FacebookSettingsDTO;
import ua.softserveinc.tc.entity.*;
import ua.softserveinc.tc.util.Security;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
@Service
public class FacebookService {

    @Autowired
    private UserDao userDao;

    @Value("${spring.social.facebook.appId}")
    private String facebookAppId;
    @Value("${spring.social.facebook.appSecret}")
    private String facebookSecret;


    public String createFacebookAuthorizationURL(){
        FacebookConnectionFactory connectionFactory = new FacebookConnectionFactory(facebookAppId, facebookSecret);
        OAuth2Operations oauthOperations = connectionFactory.getOAuthOperations();
        OAuth2Parameters params = new OAuth2Parameters();
        params.setRedirectUri("http://localhost:8080/facebook/token");
        params.setScope("public_profile,email,user_birthday,user_likes,user_friends,user_gender");
        return oauthOperations.buildAuthorizeUrl(params);
    }

    @Transactional
    public void createFacebookAccessToken(String code) {
        FacebookConnectionFactory connectionFactory = new FacebookConnectionFactory(facebookAppId, facebookSecret);
        AccessGrant accessGrant = connectionFactory.getOAuthOperations().exchangeForAccess(code, "http://localhost:8080/facebook/token", null);
        saveUserDetails(accessGrant.getAccessToken());

    }

    private void saveUserDetails(final String accessToken) {
        Facebook facebook = new FacebookTemplate(accessToken);
        String [] fields  = { "id",  "birthday", "context", "friends", "gender", "favorite_teams"};
        String userInfo = facebook.fetchObject("me", String.class, fields);
        FacebookData facebookData = new FacebookData();
        JSONObject jsonObject = new JSONObject(userInfo);
        facebookData.setId(jsonObject.getLong("id"));
        facebookData.setGender(jsonObject.getString("gender").equals("male") ? Gender.MALE : Gender.FEMALE);
        facebookData.setBirthDay(LocalDate.parse(jsonObject.getString("birthday"), DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        facebookData.setFacebookTeams(getFacebookTeams(facebookData, jsonObject.getJSONArray("favorite_teams")));
        facebookData.setFacebookLikes(getFacebookLikes(facebookData, jsonObject.getJSONObject("context").getJSONObject("mutual_likes").getJSONArray("data")));
        facebookData.setFriendsCount(jsonObject.getJSONObject("friends").getJSONObject("summary").getLong("total_count"));
        String userEmail = Security.getUserEmail();
        User userByEmail = userDao.getUserByEmail(userEmail);
        SocialData socialData = userByEmail.getSocialData() == null ? new SocialData() : userByEmail.getSocialData();
        socialData.setFacebookData(facebookData);
        userByEmail.setSocialData(socialData);
        userDao.saveOrUpdate(userByEmail);
    }

    private List<FacebookLike> getFacebookLikes(FacebookData facebookData, JSONArray likes) {
        final List<FacebookLike> result = new ArrayList<>();
        for (int i = 0; i < likes.length(); i++) {
            JSONObject likeInfo = likes.getJSONObject(i);
            final FacebookLike like = new FacebookLike();
            like.setId(likeInfo.getLong("id"));
            like.setFacebookData(facebookData);
            like.setName(likeInfo.getString("name"));
            result.add(like);
        }
        return result;
    }

    private List<FacebookTeam> getFacebookTeams(FacebookData facebookData, JSONArray teams) {
        final List<FacebookTeam> teamList = new ArrayList<>();
        for (int i = 0; i < teams.length(); i++) {
            JSONObject teamInfo = teams.getJSONObject(i);
            final FacebookTeam team = new FacebookTeam();
            team.setId(teamInfo.getLong("id"));
            team.setFacebookData(facebookData);
            team.setName(teamInfo.getString("name"));
            teamList.add(team);
        }
       return teamList;
    }

    @Transactional
    public void saveFacebookSettings(FacebookSettingsDTO dto) {
        FacebookSettings settings = new FacebookSettings();
        settings.setMaximumYearDifference(dto.getMaxYearDifference());
        settings.setMinimumFriendsNumber(dto.getMinFriendsNumbers());
        settings.setMinimumSameLikes(dto.getMinSameLikes());
        settings.setMinimumSameTeams(dto.getMinFriendsNumbers());
        settings.setGender(dto.getGender());

        String userEmail = Security.getUserEmail();
        User userByEmail = userDao.getUserByEmail(userEmail);
        SocialSettings socialSettings = userByEmail.getSocialSettings() == null ? new SocialSettings() : userByEmail.getSocialSettings();
        socialSettings.setFaceBookSettings(settings);
        userByEmail.setSocialSettings(socialSettings);
        userDao.saveOrUpdate(userByEmail);

    }
}
