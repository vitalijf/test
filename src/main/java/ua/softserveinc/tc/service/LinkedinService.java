package ua.softserveinc.tc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.linkedin.api.LinkedInProfileFull;
import org.springframework.social.linkedin.api.impl.LinkedInTemplate;
import org.springframework.social.linkedin.connect.LinkedInConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.softserveinc.tc.dao.UserDao;
import ua.softserveinc.tc.dto.LinkedinParamsDTO;
import ua.softserveinc.tc.entity.*;
import ua.softserveinc.tc.util.Security;

@Service
public class LinkedinService {

    @Value("${spring.social.linkedin.appId}")
    private String linkedinAppId;
    @Value("${spring.social.linkedin.appSecret}")
    private String linkedinSecret;

    @Autowired
    private UserDao userDao;

    public String createAuthorizationUrl(){
        LinkedInConnectionFactory connectionFactory = new LinkedInConnectionFactory(linkedinAppId, linkedinSecret);
        OAuth2Operations oauthOperations = connectionFactory.getOAuthOperations();
        OAuth2Parameters params = new OAuth2Parameters();
        params.setRedirectUri("http://localhost:8080/linkedin/token");
        params.setScope("r_emailaddress,r_basicprofile,w_share,rw_company_admin");
        return oauthOperations.buildAuthorizeUrl(params);
    }

    @Transactional
    public void createAccessToken(String code) {
        LinkedInConnectionFactory factory = new LinkedInConnectionFactory(linkedinAppId, linkedinSecret);
        AccessGrant accessGrant = factory.getOAuthOperations().exchangeForAccess(code, "http://localhost:8080/linkedin/token", null);
        String accessToken = accessGrant.getAccessToken();
        saveUserData(accessToken);

    }

    public void saveUserData(String accessToken) {
        LinkedIn linkedIn = new LinkedInTemplate(accessToken);
        LinkedInProfileFull userProfileFull = linkedIn.profileOperations().getUserProfileFull();
        final LinkedinData linkedinData = new LinkedinData();

        linkedinData.setId(userProfileFull.getId());
        linkedinData.setLocation(userProfileFull.getLocation().getCountry());
        linkedinData.setIndustry(userProfileFull.getIndustry());
        linkedinData.setPosition(userProfileFull.getHeadline());
        linkedinData.setConnectionsCount((long) userProfileFull.getNumConnections());

        String userEmail = Security.getUserEmail();
        User userByEmail = userDao.getUserByEmail(userEmail);
        SocialData socialData = userByEmail.getSocialData() == null ? new SocialData() : userByEmail.getSocialData();
        socialData.setLinkedinData(linkedinData);
        userByEmail.setSocialData(socialData);
        userDao.saveOrUpdate(userByEmail);
    }


    @Transactional
    public void saveParams(LinkedinParamsDTO dto) {
        LinkedinSettings settings = new LinkedinSettings();
        settings.setIndustry(dto.getIndustry());
        settings.setLocation(dto.getLocation());
        settings.setMinimumConnections(dto.getMinConnections());
        settings.setPosition(dto.getPosition());

        String userEmail = Security.getUserEmail();
        User userByEmail = userDao.getUserByEmail(userEmail);
        SocialSettings socialSettings = userByEmail.getSocialSettings() == null ? new SocialSettings() : userByEmail.getSocialSettings();
        socialSettings.setLinkedinSettings(settings);
        userByEmail.setSocialSettings(socialSettings);
        userDao.saveOrUpdate(userByEmail);
    }
}
