package ua.softserveinc.tc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.oauth1.OAuth1Operations;
import org.springframework.social.oauth1.OAuth1Parameters;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.RedirectView;
import ua.softserveinc.tc.dao.UserDao;
import ua.softserveinc.tc.dto.TwitterParamsDTO;
import ua.softserveinc.tc.entity.*;
import ua.softserveinc.tc.util.Security;

@Service
public class TwitterService {

    @Value("${spring.social.twitter.appId}")
    private String twitterAppId;
    @Value("${spring.social.twitter.appSecret}")
    private String twitterAppSecret;

    @Value("${spring.social.twitter.accessToken}")
    private String twitterAccessToken;
    @Value("${spring.social.twitter.tokenSecret}")
    private String tokenSecret;

    @Autowired
    private UserDao userDao;


    @Transactional
    public void createAccessToken(String oauthToken, String verifier) {
        TwitterConnectionFactory connectionFactoryTwitter = new TwitterConnectionFactory(twitterAppId,twitterAppSecret);
        OAuth1Operations oauth1Operations = connectionFactoryTwitter.getOAuthOperations();
        OAuthToken  requestToken = oauth1Operations.fetchRequestToken("http://localhost:8080/twitter/token", null);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        saveUserData();
    }



    private void saveUserData() {
        Twitter twitter = new TwitterTemplate(twitterAppId, twitterAppSecret, twitterAccessToken, tokenSecret);

        TwitterProfile userProfile = twitter.userOperations().getUserProfile();
        final TwitterData twitterData = new TwitterData();

        twitterData.setId(userProfile.getId());
        twitterData.setFriendsCount((long) userProfile.getFriendsCount());
        twitterData.setFavoritesCount((long) userProfile.getFavoritesCount());
        twitterData.setStatusesCount((long) userProfile.getStatusesCount());
        twitterData.setFollowersCount((long) userProfile.getFollowersCount());
        twitterData.setLanguage(userProfile.getLanguage());

        String userEmail = Security.getUserEmail();
        User userByEmail = userDao.getUserByEmail(userEmail);
        SocialData socialData = userByEmail.getSocialData() == null ? new SocialData() : userByEmail.getSocialData();
        socialData.setTwitterData(twitterData);
        userByEmail.setSocialData(socialData);
        userDao.saveOrUpdate(userByEmail);
    }

    @Transactional
    public String createAuthorizationUrl() {
        TwitterConnectionFactory connectionFactory = new TwitterConnectionFactory(twitterAppId, twitterAppSecret);

        OAuth1Operations oauth1Operations = connectionFactory.getOAuthOperations();

        OAuthToken requestToken = oauth1Operations.fetchRequestToken("http://localhost:8080/twitter/token", null);
        return oauth1Operations.buildAuthorizeUrl(requestToken.getValue(), OAuth1Parameters.NONE);

    }

    @Transactional
    public void saveParams(TwitterParamsDTO dto) {
        TwitterSettings settings = new TwitterSettings();
        settings.setLanguage(dto.getLanguage());
        settings.setMinimumFavoritesCount(dto.getMinFavoritesCount());
        settings.setMinimumFollowersCount(dto.getMinFollowersCount());
        settings.setMinimumFriendsCount(dto.getMinFriendsCount());
        settings.setMinimumStatusesCount(dto.getMinStatusesCount());

        String userEmail = Security.getUserEmail();
        User userByEmail = userDao.getUserByEmail(userEmail);
        SocialSettings socialSettings = userByEmail.getSocialSettings() == null ? new SocialSettings() : userByEmail.getSocialSettings();
        socialSettings.setTwitterSettings(settings);
        userByEmail.setSocialSettings(socialSettings);
        userDao.saveOrUpdate(userByEmail);
    }

}
