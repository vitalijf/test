package ua.softserveinc.tc.dao;


import org.springframework.stereotype.Repository;
import ua.softserveinc.tc.entity.FacebookLike;
import ua.softserveinc.tc.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"unchecked", "SqlDialectInspection"})
@Repository
public class SuggestedDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public List<User> getAllWithLanguage(final String language, final Long userId) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN twitter_data t on sd.twitter_data_id = t.id where t.language = :lan and u.id <> :id")
                .setParameter("lan", language)
                .setParameter("id", userId)
                .getResultList();
    }

    public List<User> getAllWithMinFriendsCount(final Long minFriendsCount, final Long userId) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN twitter_data t on sd.twitter_data_id = t.id where t.friends_count = :lan and u.id <> :id")
                .setParameter("lan", minFriendsCount)
                .setParameter("id", userId)
                .getResultList();
    }

    public List<User> getAllWithMinFollowersCount(final Long minFollowersCount, final Long userId) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN twitter_data t on sd.twitter_data_id = t.id where t.followers_count = :lan and u.id <> :id")
                .setParameter("lan", minFollowersCount)
                .setParameter("id", userId)
                .getResultList();
    }

    public List<User> getAllWithMinFavoritesCount(final Long minFavoritesCount, final Long userId) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN twitter_data t on sd.twitter_data_id = t.id where t.favorites_count = :lan and u.id <> :id")
                .setParameter("lan", minFavoritesCount)
                .setParameter("id", userId)
                .getResultList();
    }

    public List<User> getAllWithMinStatusesCount(final Long minStatusesCount, final Long userId) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN twitter_data t on sd.twitter_data_id = t.id where t.statuses_count = :lan and u.id <> :id")
                .setParameter("lan", minStatusesCount)
                .setParameter("id", userId)
                .getResultList();
    }

    public List<User> getAllWithIndustry(String industry, Long id) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN linkedin_data t on sd.twitter_data_id = t.id where t.industry = :lan and u.id <> :id")
                .setParameter("lan", industry)
                .setParameter("id", id)
                .getResultList();
    }

    public List<User> getAllWithLocation(String location, Long id) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN linkedin_data t on sd.twitter_data_id = t.id where t.location = :lan and u.id <> :id")
                .setParameter("lan", location)
                .setParameter("id", id)
                .getResultList();
    }

    public List<User> getAllWithPosition(String position, Long id) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN linkedin_data t on sd.twitter_data_id = t.id where t.position = :lan and u.id <> :id")
                .setParameter("lan", position)
                .setParameter("id", id)
                .getResultList();
    }

    public List<User> getAllWithMinConnections(Long minConnections, Long id) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN linkedin_data t on sd.twitter_data_id = t.id where t.minConnections = :lan and u.id <> :id")
                .setParameter("lan", minConnections)
                .setParameter("id", id)
                .getResultList();
    }

    public List<User> getAllWithGender(String gender, Long id) {
       return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN linkedin_data t on sd.facebook_data_id = t.id where t.gender = :lan and u.id <> :id")
                .setParameter("lan", gender)
                .setParameter("id", id)
                .getResultList();
    }

    public List<User> getAllWithMinFriendsCountFB(Long minFriendsCount, Long id) {
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN linkedin_data t on sd.facebook_data_id = t.id where t.friends_count = :lan and u.id <> :id")
                .setParameter("lan", minFriendsCount)
                .setParameter("id", id)
                .getResultList();
    }

    public List<User> getAllWithMaximumYearDifference(Long maximumYearDifference, User user) {

        LocalDate birthDay = user.getSocialData().getFacebookData().getBirthDay();
        return entityManager.createNativeQuery("SELECT u.* FROM users u INNER JOIN social_data sd ON u.social_data_id = sd.id " +
                "INNER JOIN linkedin_data t on sd.facebook_data_id = t.id where ABSOLUTE (DATEDIFF('year' t.birthday, :birthday) <= :lan and u.id <> :id")
                .setParameter("lan", maximumYearDifference)
                .setParameter("id", user.getId())
                .setParameter("birthDay", birthDay)
                .getResultList();
    }

    public List<User> getAllWithSameLikes(Long minimumSameLikes, User user) {
    return new ArrayList<>();
    }

    public List<User> getAllWithMiniSameTeams(Long minimumSameTeams, User user) {
        return new ArrayList<>();
    }
}
