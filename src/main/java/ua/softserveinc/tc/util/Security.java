package ua.softserveinc.tc.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;


public class Security {

    public static String getUserEmail() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ((User) principal).getUsername();
    }
}
