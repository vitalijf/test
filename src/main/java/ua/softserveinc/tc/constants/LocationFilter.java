package ua.softserveinc.tc.constants;

public enum  LocationFilter {

    SAME_CITY, SAME_COUNTRY, IGNORE_LOCATION
}
